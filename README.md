# Favorite Medium's Stylelint Plugin

_Supercharged css, scss, less language style guide._

## Getting started

Getting start is very simple; it is just 2 step process:

### 1. Installation

First you need to install `stylelint` & `stylelint-config-favoritemedium` modules as dev-dependencies, use one of following command based upon your package manager configurations:

```sh
# npm
npm i -D stylelint stylelint-config-favoritemedium

# yarn
yarn add -D stylelint stylelint-config-favoritemedium
```

### 2. Setup in project

Create `.stylelintrc.json` file in project source directory and extend rules from installed plugin like following:

```js
// .stylelintrc.json
{
  "extends": "stylelint-config-favoritemedium"
}
```

now navigate to `package.json` file and add following into `scripts` section:

```js
// package.json

"scripts": {
  "lint:style": "stylelint ./src"
},

```

#### Pro tips (optional but nice to have)

##### Controlling stylelint

Add `.stylelintignore` file into project root; to control style lint running on unwanted files and directories.

```
# .stylelintignore

node_modules
package.json
```

##### Performance boost

Use `--cache` flag with lint command; i.e.

```js
  "lint:style": "stylelint ./src --cache" // note -cache flag
```

with this flag stylelint will ignore unchanged files and scan modified files only.

Upon running this command plugin will generate `.stylelintcache` file in project root and you might need to add that into `.gitignore` to avoid pushing to remote.

### 3. Running

Once you setup script into `package.json` file create some css files and run command `npm run lint:style` or `yarn lint:style`; you will be prompted with violations messages in console if there are any!

![image](https://user-images.githubusercontent.com/6498132/72588200-5e765380-3919-11ea-835b-9f1f8840b4a7.png)

## Contributions

You are welcome to contribute this (css, scss, less) language style guide line plugin.

For contribution you can either report an issue or fork project and create Pull Request. We will just review and merge proposed changes.

## License

This project is licensed under MIT
